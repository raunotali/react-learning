import React, { Component } from 'react';
import UserOutput from './components/UserOutput/UserOutput';
import './App.css';
import UserInput from './components/UserInput/UserInput';

class App extends Component {

  state = {
    username: "pede"
  }

  userNameHandler = () => {
    this.setState({
      username: "rauno"
    })
  }

  render() {
    return (
      <div className="App">
        <button onClick={this.userNameHandler}>Press me</button>
        <UserInput click={this.userNameHandler} />
        <UserOutput username={this.state.username}  />
        <UserOutput />
        <UserOutput />
      </div>
    );
  }
}

export default App;
