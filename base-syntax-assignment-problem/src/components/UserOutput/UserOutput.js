import React, { Component } from 'react';
import './UserOutput.css';

const UserOutput = props => {

    return (
      <div className="user-output">
        <p>Username: {props.username ? props.username : "No username"}</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed euismod nisi porta lorem.</p>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed euismod nisi porta lorem.</p>
      </div>
    );
}

export default UserOutput;
