import React, { Component } from 'react';
import './UserInput.css';

class UserInput extends Component {
  render() {
    return (
      <div className="user-input">
        <input />
      </div>
    );
  }
}

export default UserInput;
